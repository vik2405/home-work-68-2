import {
  FETCH_LIST_SUCCESS,
  FETCH_LIST_REQUEST,
  FETCH_LIST_ERROR,
  CHANGE_VALUE
} from "./actions";

const initialState = {
  list: {},
  currentTask: "",
  loading: false,
  error: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LIST_SUCCESS:
      return { ...state, list: action.list, loading: false };
    case FETCH_LIST_REQUEST:
      return { ...state, loading: true };
    case FETCH_LIST_ERROR:
      return { ...state, error: true };
    case CHANGE_VALUE:
      return { ...state, currentTask: action.value };

    default:
      return state;
  }
};

export default reducer;
