import axios from "./axios-list";

export const FETCH_LIST_REQUEST = "FETCH_LIST_REQUEST";
export const FETCH_LIST_SUCCESS = "FETCH_LIST_SUCCESS";
export const FETCH_LIST_ERROR = "FETCH_LIST_ERROR";
export const CHANGE_VALUE = "CHANGE_VALUE";

export const fetchListRequest = () => {
  return { type: FETCH_LIST_REQUEST };
};

export const fetchListSuccess = list => {
  return { type: FETCH_LIST_SUCCESS, list: list };
};

export const fetchListError = () => {
  return { type: FETCH_LIST_ERROR };
};

export const fetchList = () => {
  return dispatch => {
    dispatch(fetchListRequest());
    axios.get("/list.json").then(
      response => {
        console.log(response.data);
        dispatch(fetchListSuccess(response.data));
      },
      error => {
        dispatch(fetchListError());
      }
    );
  };
};

export const changeValue = value => {
  return { type: CHANGE_VALUE, value: value };
};

export const enterTodo = value => {
  return dispatch => {
    dispatch(fetchListRequest());
    axios .post("/list.json", { task: value }) 
    .then(response => {
      console.log('post response', response.data)
      dispatch(fetchList());
    }),
    error => dispatch(fetchListError());
};
};

     