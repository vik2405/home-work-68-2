import React, { Component } from "react";
import "./List.css";
import { connect } from "react-redux";
import { fetchList, enterTodo , changeValue} from "../../store/actions";

class List extends Component {
  componentDidMount() {
    this.props.fetchList();
  }

  render() {
    return (
      <div className="List">
        <h3>MAKE</h3>
        <input
          type="input"
          placeholder="TODO"
          className="enter"
          value={this.props.currentTask}
          onChange={this.props.changeValue}
        />
        <button className="send" onClick={() => this.props.enterTodo(this.props.currentTask)}>Send</button> 
        <h3>TASKS</h3>
        <div className="text">{Object.values(this.props.list).map((item, index) => (
          <p key={index}>{item.task}</p>
        ))}</div>
      </div>
    );
  }
}


const mapStateToProps = state => state;
const mapDispatchToProps = dispatch => {
  return {
    enterTodo: value => dispatch(enterTodo(value)),
    fetchList: () => dispatch(fetchList()),
    changeValue: (e) => dispatch(changeValue(e.target.value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);


